package jp.alhinc.tsuji_yuki.calculate_sales;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class CalculateSales {
	public static void main(String[]args) throws IOException{
		HashMap<String,String> codeNameMap = new HashMap<String,String>();
		HashMap<String,Long> codeTotalMoneyMap = new HashMap<String,Long>();
		List<String> nameList = new ArrayList<String>();
		List<Integer> checkList = new ArrayList<Integer>();
		BufferedReader nameListBr = null;
		if(args.length != 1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		int serialNumberInt = 0;
		File files = new File(args[0]);
		String[] fileNames = files.list();
		String path = new String(args[0]);
		String outBranchFile = "branch.out";
		String listFileName = "branch.lst";
		String branchCode = "[0-9]{3},.*";
		if(!lradList(codeTotalMoneyMap,codeNameMap,path,listFileName,branchCode)){
			return;
		}
		try{
			for(int i = 0; i < fileNames.length; i++){
				if( fileNames[i].matches("[0-9]{8}.rcd")){
					File rcdFile = new File(path,fileNames[i]);
					if( rcdFile.isFile() ){
						nameList.add(fileNames[i]);
					}
				}
			}
			for(int i = 0;i < nameList.size(); i++){
				String[]serialNumber =  nameList.get(i).split(".rcd");
				serialNumberInt = Integer.parseInt( serialNumber[0] );
				checkList.add(serialNumberInt);
				if(	checkList.size() > 1){
					if( !( checkList.get(i) - 1 == checkList.get(i - 1)) ){
						System.out.println("売上ファイル名が連番になっていません");
						return;
					}
				}
			}
			long codeMoneyInt = 0;
			for(int t = 0;t < nameList.size(); t++){
				List<String> codeMoney = new ArrayList<String>();
				File fileName = new File(path,nameList.get(t));
				FileReader fr = new FileReader(fileName);
				nameListBr	= new BufferedReader(fr);
				String line;

				while( (line = nameListBr.readLine() ) != null){
					codeMoney.add(line);
				}
				if(codeMoney.size() != 2){
					System.out.println(nameList.get(t)+"のフォーマットが不正です");
					return;
				}
				if(!(codeTotalMoneyMap.containsKey(codeMoney.get(0)))){
					System.out.println(nameList.get(t)+"の支店コードが不正です");
					return;
				}
				if(!(codeMoney.get(1).matches("[0-9]{1,}"))){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				long moneyTotal = 0;
				codeMoneyInt = Long.parseLong( codeMoney.get(1) );
				moneyTotal = codeMoneyInt + codeTotalMoneyMap.get(codeMoney.get(0));

				if(moneyTotal >= 10000000000l){
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				codeTotalMoneyMap.put(codeMoney.get(0),moneyTotal);
			}
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}finally{
			if(nameListBr != null ){
				nameListBr.close();
			}
		}
		if(!totalWrite(codeTotalMoneyMap,codeNameMap,path,outBranchFile)){
			return;
		}
	}
	public static boolean lradList(HashMap<String, Long> codeTotalMoneyMap, HashMap<String, String> codeNameMap,
			String path, String listFileName, String branchCode) throws IOException{
		BufferedReader codeNameBr = null;
		try{
			File branchFile = new File(path,listFileName);
			if (!(branchFile.exists())){
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(branchFile);
			codeNameBr = new BufferedReader(fr);
			String branchList;

			while((branchList = codeNameBr.readLine()) != null){
				if( !(branchList.matches(branchCode))){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				String[] data =  branchList.split(",");
				if( data.length > 2){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}
				codeNameMap.put(data[0],data[1]);
				codeTotalMoneyMap.put(data[0],(long) 0);
			}
		}catch(FileNotFoundException er){
			System.out.println("支店定義ファイルが存在しません");
			return false;
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally{
			if(codeNameBr != null){
				codeNameBr.close();
			}
		}
		return true;
	}
	private static boolean totalWrite(HashMap<String, Long> codeTotalMoneyMap, HashMap<String, String> codeNameMap,
			String path, String outBranchFile) {
		PrintWriter printOut = null;
		try{
			File outfile = new File(path,outBranchFile);
			FileWriter fw = new FileWriter(outfile,false);
			printOut = new PrintWriter(new BufferedWriter(fw));

			for (String codeKey : codeTotalMoneyMap.keySet()){
				printOut.println(codeKey+","+codeNameMap.get(codeKey)+","+codeTotalMoneyMap.get(codeKey));
			}
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally{
			if(printOut != null){
				printOut.close();
			}
		}
	return true;
	}
}
