package jp.alhinc.tsuji_yuki.a.calculate_sales;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//jp.alhinc.tsuji_yuki.calculate_sales
public class CalculateSales {
	public static void main(String[]args){

		System.out.println("ファイルを読み込みます->"+args[0]);
		HashMap<String,String> codeNameMap = new HashMap<String,String>();
		HashMap<String,Integer> codeTotalMoneyMap = new HashMap<String,Integer>();
		//HashMap<String,String> codeMoneyMap = new HashMap<String,String>();
		File file = new File(args[0]);
		String[] filename = file.list();
		List<String> codeMoney = new ArrayList<String>();
		int total = 0;


		//支店ファイル読み込み
		try{

			File branchFile = new File(args[0],"branch.lst");
			FileReader fr = new FileReader(branchFile);
			BufferedReader br = new BufferedReader(fr);
			String branchList;
			while((branchList = br.readLine()) != null){
				System.out.println(branchList);
				//カンマで区切る
				String[] data =  branchList.split(",");
				//000,支店名をマップin
				codeNameMap.put(data[0],data[1]);
				//支店コードと金額を紐付け,0を作成
				codeTotalMoneyMap.put(data[0],0);
				//コード単体保管
				// code = data[0];

				}
			br.close();
		}catch(Exception er){
			System.out.println("支店定義ファイルが存在しません");
		}

		System.out.println("---------------------------------------");
		//キャスト用
		int codeMoney2 = 0;
		//奇数偶数判別用
		int c =0;


		try{
		for(int i=0;i<filename.length;i++){
			//条件にあてはまるように選別
			if( filename[i].matches(".*.rcd.*$") == true){
					if(filename[i].matches("^.*[0-9]{8}.*") == true){
					
						System.out.println(filename[i] + "を読み込みます");

						//条件に当てはまったものを読み込む
						File file2 = new File(args[0],filename[i]);
						FileReader fr = new FileReader(file2);
						BufferedReader br	= new BufferedReader(fr);
						String line;
		
						while((line = br.readLine()) != null){
							System.out.println(line);
							codeMoney.add(line);
						}
							
						System.out.println("---------------------------------------");
						
						
						//codeMoney(1)をIntegerへ
						codeMoney2 = Integer.parseInt( codeMoney.get(1+c) );
						//合計の値をだす
						total = codeMoney2 + codeTotalMoneyMap.get(codeMoney.get(c));
						//System.out.println("合計値は" + total);
						codeTotalMoneyMap.put(codeMoney.get(c),total);
						System.out.println("支店コード"+codeMoney.get(c)+ "の合計金額は"+codeTotalMoneyMap.get(codeMoney.get(c)));
						c=c+2;
						br.close();
						System.out.println("---------------------------------------");
				}	
			}
		}
		
		//ファイル出力	branch.out パス
		File outfile = new File(args[0],"branch.out");
		//ファイル作成
		FileWriter fw = new FileWriter(outfile,false);
		//ファイル書き込み
		PrintWriter pw = new PrintWriter(new BufferedWriter(fw));
		//コードか金額かの判別用
		int n = 0;
		for(int t=0;t<codeTotalMoneyMap.size();t++){
			pw.println(codeMoney.get(n)+","+codeNameMap.get(codeMoney.get(n))+","+codeTotalMoneyMap.get(codeMoney.get(n)));
		n=n+2;
		}
		
		
		pw.close();

		System.out.println("書き込みが完了しました");

		}catch(Exception e){
		System.out.println("予期せぬエラーが発生しました"+e);
		}


	}

}





